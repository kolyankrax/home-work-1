<?php
session_start();

function addLog(string $file, string $str) {
    $f = fopen($file, 'ab');
    fwrite($f, $str . PHP_EOL);
    fclose($f);
}

function login($login, $password): bool
{
    if (
        $login == 'admin'
        && $password == 'test'
    ) {
        $_SESSION['user'] = [
            'name' => $login
        ];

        date_default_timezone_set('ASIA/Novosibirsk');

        addLog(
            'login-log',
            sprintf('%s logged in on %s', $login, date('Y-m-d H:i:s T'))
        );

        return true;
    }

    return false;
}

function logout()
{
    session_destroy();
    header('Location: /index.php');
    exit();
}

function getUserName(): string
{
    return $_SESSION['user']['name'] ?? 'unknown';
}

function redirectOnLogin($loginResult)
{
    if ($loginResult) {
        header('Location: /success.php');
    } else {
        header('Location: /fail.php');
    }
    exit();
}
